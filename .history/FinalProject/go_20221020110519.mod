module FinalProject

go 1.19

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	gorm.io/gorm v1.24.0
)

require (
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)
